---
title: "Graphics"
author: "Antoine Lamer"
date: "14 septembre 2020"
output:
  ioslides_presentation: default
  beamer_presentation: default
---

<style>
html { font-size: 1em }
body {
text-align: justify}
</style>


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_chunk$set(warning = FALSE)
knitr::opts_chunk$set(message = FALSE)

library(dplyr)
library(ggplot2)
##library(ggvis)
```

```{r data_reading, message=FALSE, echo = FALSE, cache = TRUE}

file_src = read.csv2("data.csv")

```

```{r data_management, message=FALSE, echo = FALSE, dependson="data_reading", cache= TRUE}

data_src = file_src

colnames(data_src) = tolower(colnames(data_src))

data_src$sexe = gsub('0', 'Male', data_src$sexe)
data_src$sexe = gsub('1', 'Female', data_src$sexe)
data_src$sexe = factor(data_src$sexe)

data_src$asa = factor(data_src$asa)
levels(data_src$asa) = c("ASA1", "ASA2", "ASA3", "ASA4", "ASA5")

##data_src$mois_fr = ordered(data_src$mois_fr, levels=c("Jan", "Fév", "Mar", "Avr", "Mai", "Jun", "Jui", "Aou", "Sep", "Oct", "Nov", "Dec"))

## Fin du data management

data = data_src

data_min = data %>%
        select(c(age, poids, taille, categorie_asa, asa, sexe, trimestre, annee, duree_anesthesie,
                 lib_chapitre, duree_sejour_interv, deces_sejour_interv,
                 rea_si_post_op, nb_fcecg_chir, moy_fcecg_chir,
                 nb_pam_chir, moy_pam_chir, nb_spo2_chir, moy_spo2_chir)) %>%
        filter(!is.na(age)) %>%
        filter(!is.na(poids)) %>%
        filter(!is.na(taille)) %>%
        filter(!is.na(asa)) %>%
        filter(!is.na(sexe)) %>%
        filter(duree_sejour_interv < 30)

```

## Objectives

<span style="font-size: 8pt">

* **explore data** after loading, during manipulation and computing new variables

* **show comparisons** during exploratory analysis

* **show causality**

How will the plot be used ?

* to check data  
* to be printed  
* to be displayed during a presentation  
* ...

</span>

## Devices

There are two types of devices to display and to store graphics :

* **bitmap** : PNG, JPEG/JPG, BMP, TIFF. Images are stored into dot-patern formats.

* **vector format** : PDF, SVG (scalable vector graphic, format libre), Encapsulated PostScript, windows meta-file. This format allows resizing the graphics without losing image quality.

Plots are created in two steps :

* creation of the plot
* annotation of the plot

## Graphics elements

* Data (single or multiple series)
* Type of graphics
* Axis : x, y, z
* Origin
* Title
* Legend
* Tags

<img src="images/okinawa_diet.jpeg" alt="drawing" style="max-width:50%;"/>

## Exploratory graphics

Exploratory graphics are useful to understand data, to validate data transformations, 
to explore trends ...

There are quickly developed and not used to communicate.

R *graphics* package provides functions for basic graphics.

```{r hist, echo=TRUE, out.width="300px", dependson='data_management', cache = TRUE}
hist(data_min$duree_sejour_interv, main = "Length of stay")
```

## Histogram

Set **breaks** parameter.

```{r hist_2, echo=TRUE, out.width="400px", dependson='data_management', cache = TRUE}

hist(data_min$duree_sejour_interv, breaks = 30, 
     main="Length of stay", xlab = "Length of stay (day)")        

```

## Boxplot

```{r boxplot, echo=TRUE, out.width="300px", dependson='data_management', cache = TRUE}
boxplot(data_min$duree_sejour_interv)
```

```{r boxplot_2, echo=TRUE, out.width="300px", dependson='data_management', cache = TRUE}
boxplot(data$age ~ data$asa, xlab="ASA Status", ylab="Length of stay")
```

## Barplot

```{r barplot_2, out.width="400px", dependson='data_management', cache = TRUE}
barplot(table(data$asa), main = "Interventions per ASA Status")
```

## Plot

```{r plot, out.width="400px", dependson='data_management', cache = TRUE}

plot(data$taille, data$poids, main =
"Weight ~ Height", xlab="height", ylab="weight"
)
```

## Plot

**abline** adds line to the plot

```{r plot_line, out.width="400px", dependson='data_management', cache = TRUE}
plot(data$taille, data$poids, main =
"Weight ~ Height", xlab="height", ylab="weight"
)

model <- lm(poids ~ taille, data)
abline(model, lwd = 2, col="blue")
##abline(10)
##abline(100)
```

## Multiple plots

**par** with parameters mfcol and mfrow organises presention of the plots (see documentation).

```{r multiple_plot, out.width="400px", dependson='data_management', cache = TRUE}
par(mfrow = c(1,2))

boxplot(data_min$duree_sejour_interv ~ data_min$annee)

p_deces = prop.table(table(data_min$deces_sejour_interv, data_min$categorie_asa), 2)*100
barplot(p_deces[2,])
```

## Plot settings

* **main** for the title of the plot
* **sub** for the title of the plot: see title.
* **xlab** for the title of the x axis
* **ylab** for the title of the y axis
* **asp** for the y/x aspect ratio

For more information : 
```{r information, eval=FALSE, echo=TRUE, dependson='data_management', cache = TRUE}
library(help = "graphics")
```

## ggplot2

* qplot()

* ggplot()

* aesthetics (size, shape, color)

* geoms (points, line)

## Qplot

```{r qplot, out.width="400px", dependson='data_management', cache = TRUE}

qplot(taille, poids, data=data_min)

```

## Qplot - geom

```{r qplot_geom, out.width="400px", dependson='data_management', cache = TRUE}

qplot(taille, poids, data=data_min, geom=c("point", "smooth"))

```

## Qplot - Density

```{r qplot_density, out.width="400px", dependson='data_management', cache = TRUE}

qplot(age, data=data_min, geom="density")

```

## Qplot - color

```{r qplot_color_2, out.width="400px", dependson='data_management', cache = TRUE}

qplot(age, data=data_min, color=sexe, geom="density")

```

## Qplot - fill

```{r qplot_fill, out.width="400px", dependson='data_management', cache = TRUE}
qplot(duree_sejour_interv, data=data_min, fill=asa)
```

## Qplot - Facets

facet=.~sexe or facet=sexe~.

```{r qplot_facets, out.width="400px", dependson='data_management', cache = TRUE}
qplot(taille, poids, data=data_min, facets=sexe~.)

```

## ggplot2

* A data frame 
* **aesthetic** precies how data are mapped to color, size  
* **geoms** defines geometric objects like points, lines, shapes 
* **facets** partitions a plot into a matrix of plots 
* **stats** performs statistical transformations like smoothing 
* **scales** what scale an aesthetic map uses (example: male = red, female = blue).  
* coordinate system

ggplot(data) + geom_function() +  
coordinate_function() +  
facet_function() +  
scale_function() +  
theme_function()  


## ggplot2 - ggplot function

```{r histogram_ggplot2, out.width="400px", dependson='data_management', cache = TRUE}

g = ggplot(data_min, aes(x=duree_sejour_interv)) 

summary(g)
```

## geom_histogram

```{r geom_histogram, out.width="400px", dependson='data_management', cache = TRUE}

g + geom_histogram(aes(y=..density..)) 

```

## geom_density


```{r geom_density, out.width="400px", dependson='data_management', cache = TRUE}

ggplot(data_min, aes(age, colour = sexe)) +
  geom_density(adjust = 1)
        
```

## geom_point

```{r geom_point, out.width="400px", dependson='data_management', cache = TRUE}

ggplot(data_min, aes(taille, poids, colour = sexe, na.rm = TRUE)) +
  geom_point()
        
```

## ggplot2 - geom_point

```{r geom_point_2, out.width="400px", dependson='data_management', cache = TRUE}

g = ggplot(data_min, aes(taille, poids, colour = sexe, na.rm = TRUE))

g + geom_point(size=0.5)
```

geom_point understands the following aesthetics :

* x

* y

* alpha

* colour

* fill

* group

* shape

* size

* stroke

## ggplot2 - add layers

```{r geom_point_layer, out.width="400px", dependson='data_management', cache = TRUE}

g = ggplot(data_min, aes(taille, poids, colour = sexe, na.rm = TRUE))

g + geom_point() + geom_smooth(method = "lm")
```

## ggplot2 - title

```{r geom_point_title, out.width="400px", dependson='data_management', cache = TRUE}

g = ggplot(data_min, aes(taille, poids, colour = sexe))

g + geom_point() + labs(x = 'Height (cm)', y = 'Weight (kg))') +
        theme(legend.position="top")
```

## ggplot2 - save

```{r geom_point_save, out.width="400px", dependson='data_management', cache = TRUE}

ggsave("plot_ggplot2.png", width=5, height=5)

```

## Resources

* R graphics cookbook, Winston Chang

* https://ggplot2.tidyverse.org/

* https://github.com/tidyverse/ggplot2/wiki

* https://github.com/rstudio/cheatsheets/blob/master/data-visualization-2.1.pdf

* https://www.data-to-viz.com



